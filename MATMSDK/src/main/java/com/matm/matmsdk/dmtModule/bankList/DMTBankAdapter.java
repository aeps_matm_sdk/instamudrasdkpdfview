package com.matm.matmsdk.dmtModule.bankList;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.RadioButton;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import java.util.ArrayList;
import java.util.List;

import isumatm.androidsdk.equitas.R;

public class DMTBankAdapter extends RecyclerView.Adapter<DMTBankAdapter.DMTBankHolder> implements Filterable {
    private int lastSelectedPosition = -1;

    public List<DMTBankModel> bankNameModelList, filterList;
    DMTBankAdapter.RecyclerViewClickListener recyclerViewClickListener;

    public DMTBankAdapter(List<DMTBankModel> bankNameModelList, RecyclerViewClickListener recyclerViewClickListener) {
        this.bankNameModelList = bankNameModelList;
        this.filterList = bankNameModelList;
        this.recyclerViewClickListener = recyclerViewClickListener;
    }

    public DMTBankModel getItem(int position) {
        return filterList.get(position);
    }

    @NonNull
    @Override
    public DMTBankHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.dmt_bank_list_item, parent, false);
        return new DMTBankHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull DMTBankHolder holder, int position) {
        DMTBankModel current = filterList.get(position);
        holder.bankName.setText(current.getBankName());
        holder.bankName.setChecked(lastSelectedPosition == position);
    }

    @Override
    public int getItemCount() {
        return filterList.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {


                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    filterList = bankNameModelList;
                } else {
                    List<DMTBankModel> filteredList = new ArrayList<>();
                    for (DMTBankModel row : bankNameModelList) {

                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.getBankName().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                        }
                    }

                    filterList = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = filterList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                if (filterResults.values != null) {

                    filterList = (ArrayList<DMTBankModel>) filterResults.values;
                    notifyDataSetChanged();
                }
            }
        };
    }

    public interface RecyclerViewClickListener {

        public void recyclerViewListClicked(View v, int position);
    }

    public class DMTBankHolder extends RecyclerView.ViewHolder {
        public RadioButton bankName;

        public DMTBankHolder(@NonNull View itemView) {
            super(itemView);
            bankName = (RadioButton) itemView.findViewById(R.id.bankName);
            bankName.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    lastSelectedPosition = getAdapterPosition();
                    notifyDataSetChanged();
                    recyclerViewClickListener.recyclerViewListClicked(v, lastSelectedPosition);

                }
            });
        }
    }
}

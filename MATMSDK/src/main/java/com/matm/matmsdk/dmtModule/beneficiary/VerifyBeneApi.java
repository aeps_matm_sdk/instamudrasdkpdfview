package com.matm.matmsdk.dmtModule.beneficiary;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Url;

public interface VerifyBeneApi {
    @POST()
    Call<VerifyBeneResponse> getVerifyBene(@Header("Authorization") String token, @Body VerifyBeneRequest verifyBeneRequestBody, @Url String url);
}

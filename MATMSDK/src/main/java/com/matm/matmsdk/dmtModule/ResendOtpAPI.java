package com.matm.matmsdk.dmtModule;

import retrofit2.Call;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Url;

public interface ResendOtpAPI {
    @POST()
    @Headers("Content-Type: application/json")
    Call<ResendOtpResponse> getResendOtpReport(@Header("Authorization") String token, @Url String url);

}

package com.matm.matmsdk.dmtModule;

public class AddCustomerRequest {

    public AddCustomerRequest(String customerName, String customerNo) {
        this.customerName = customerName;
        this.customerNo = customerNo;
    }

    private String customerName;

    private String customerNo;

    public String getCustomerName ()
    {
        return customerName;
    }

    public void setCustomerName (String customerName)
    {
        this.customerName = customerName;
    }

    public String getCustomerNo ()
    {
        return customerNo;
    }

    public void setCustomerNo (String customerNo)
    {
        this.customerNo = customerNo;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [customerName = "+customerName+", customerNo = "+customerNo+"]";
    }

}

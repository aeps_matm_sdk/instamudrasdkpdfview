package com.matm.matmsdk.aepsmodule.transactionstatus;

import android.Manifest;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.draw.LineSeparator;
import com.matm.matmsdk.ChooseCard.ChooseCardActivity;
import com.matm.matmsdk.FileUtils;
import com.matm.matmsdk.Utils.SdkConstants;
import com.matm.matmsdk.dmtModule.bluetooth.BluetoothPrinter;
import com.matm.matmsdk.permission.PermissionsActivity;
import com.matm.matmsdk.permission.PermissionsChecker;
import com.matm.matmsdk.readfile.ShowPDFActivity;
import com.paxsz.easylink.api.EasyLinkSdkManager;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import isumatm.androidsdk.equitas.R;

import static com.matm.matmsdk.permission.PermissionsActivity.PERMISSION_REQUEST_CODE;
import static com.matm.matmsdk.permission.PermissionsChecker.REQUIRED_PERMISSION;


public class TransactionStatusNewActivity extends AppCompatActivity {

    private RelativeLayout ll_maiin;
    LinearLayout detailsLayout;
    private TextView statusMsgTxt, statusDescTxt;
    private ImageView status_icon;
    private TextView date_time, rref_num, aadhar_number, bank_name, card_amount, card_transaction_type, card_transaction_amount, txnID;
    public EasyLinkSdkManager manager;
    private Button backBtn,downloadBtn,printBtn;
    //LinearLayout ll13, ll12;
    String balance = "N/A";
    String amount = "N/A";
    String referenceNo = "N/A";
    String bankName = "N/A";
    String aadharCard = "N/A";
    String txnid = "N/A";
    PermissionsChecker checker;
    Context mContext;
    private int STORAGE_PERMISSION_CODE=1;
    BluetoothAdapter B;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        setContentView(R.layout.activity_transaction_status_aeps1);

        checker = new PermissionsChecker(this);
        mContext = getApplicationContext();


        ll_maiin = findViewById(R.id.ll_maiin);
        //ll13 = findViewById(R.id.ll13);
        //ll12 = findViewById(R.id.ll12);
        B= BluetoothAdapter.getDefaultAdapter();
        detailsLayout = findViewById(R.id.detailsLayout);
        txnID = findViewById(R.id.txnID);
        card_transaction_amount = findViewById(R.id.card_transaction_amount);
        statusMsgTxt = findViewById(R.id.statusMsgTxt);
        status_icon = findViewById(R.id.status_icon);
        aadhar_number = findViewById(R.id.aadhar_number);
        rref_num = findViewById(R.id.rref_num);
        bank_name = findViewById(R.id.bank_name);
        card_transaction_type = findViewById(R.id.card_transaction_type);
        card_amount = findViewById(R.id.card_amount);
        statusDescTxt = findViewById(R.id.statusDescTxt);
        backBtn = findViewById(R.id.backBtn);
        downloadBtn = findViewById(R.id.downloadBtn);
        printBtn = findViewById(R.id.printBtn);
        date_time = findViewById(R.id.date_time);

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd : HH.mm.ss");
        String currentDateandTime = sdf.format(new Date());
        date_time.setText(currentDateandTime);
        if (getIntent().getSerializableExtra(SdkConstants.TRANSACTION_STATUS_KEY) == null) {
            ll_maiin.setBackgroundColor(getResources().getColor(R.color.statusfail));
            status_icon.setImageResource(R.drawable.ic_errorrr);
            statusMsgTxt.setText("Failed.");
            detailsLayout.setVisibility(View.GONE);
            backBtn.setBackgroundResource(R.drawable.button_background_fail);
            downloadBtn.setBackgroundResource(R.drawable.button_background_fail);
            printBtn.setBackgroundResource(R.drawable.button_background_fail);
        } else {
            TransactionStatusModel transactionStatusModel = (TransactionStatusModel) getIntent().getSerializableExtra(SdkConstants.TRANSACTION_STATUS_KEY);

            if (transactionStatusModel.getStatus().trim().equalsIgnoreCase("0")) {

                aadharCard = transactionStatusModel.getAadharCard();
                if (transactionStatusModel.getAadharCard() == null) {
                    aadharCard = "N/A";
                } else {
                    if (transactionStatusModel.getAadharCard().equalsIgnoreCase("")) {
                        aadharCard = "N/A";
                    } else {
                        StringBuffer buf = new StringBuffer(aadharCard);
                        buf.replace(0, 10, "XXXX-XXXX-");
                        System.out.println(buf.length());
                        aadharCard = buf.toString();
                    }
                }
                if (transactionStatusModel.getTxnID() != null && !transactionStatusModel.getTxnID().matches("")) {
                    txnid = transactionStatusModel.getTxnID();
                }
                if (transactionStatusModel.getBankName() != null && !transactionStatusModel.getBankName().matches("")) {
                    bankName = transactionStatusModel.getBankName();
                }
                if (transactionStatusModel.getReferenceNo() != null && !transactionStatusModel.getReferenceNo().matches("")) {
                    referenceNo = transactionStatusModel.getReferenceNo();
                }

                if (transactionStatusModel.getBalanceAmount() != null && !transactionStatusModel.getBalanceAmount().matches("")) {
                    balance = transactionStatusModel.getBalanceAmount();
                    if (balance.contains(":")) {
                        String[] separated = balance.split(":");
                        balance = separated[1].trim();
                    }
                }

                if (transactionStatusModel.getTransactionAmount() != null && !transactionStatusModel.getTransactionAmount().matches("")) {
                    amount = transactionStatusModel.getTransactionAmount();
                }
                if (transactionStatusModel.getTransactionType().equalsIgnoreCase("Cash Withdrawal")) {
                    //ll13.setVisibility(View.VISIBLE);
                    txnID.setText(txnid);
                    rref_num.setText(referenceNo);
                    aadhar_number.setText(aadharCard);
                    bank_name.setText(bankName);
                    card_amount.setText(balance);
                    card_transaction_amount.setText(amount);
                    card_transaction_type.setText(transactionStatusModel.getTransactionType());

                } else if (transactionStatusModel.getTransactionType().equalsIgnoreCase("Balance Enquery")||transactionStatusModel.getTransactionType().equalsIgnoreCase("Balance Enquiry")) {

                    txnID.setText(txnid);
                    rref_num.setText(referenceNo);
                    aadhar_number.setText(aadharCard);
                    bank_name.setText(bankName);
                    card_amount.setText(balance);
                    card_transaction_amount.setText(amount);
                    card_transaction_type.setText("Balance Enquiry");

                }
            } else {
                String aadharCard = transactionStatusModel.getAadharCard();
                if (transactionStatusModel.getAadharCard() == null) {
                    aadharCard = "N/A";
                } else {
                    if (transactionStatusModel.getAadharCard().equalsIgnoreCase("")) {
                        aadharCard = "N/A";
                    } else {
                        StringBuffer buf = new StringBuffer(aadharCard);
                        buf.replace(0, 10, "XXXX-XXXX-");
                        System.out.println(buf.length());
                        aadharCard = buf.toString();
                    }
                }
                if (transactionStatusModel.getTxnID() != null && !transactionStatusModel.getTxnID().matches("")) {
                    txnid = transactionStatusModel.getTxnID();
                }
                if (transactionStatusModel.getBankName() != null && !transactionStatusModel.getBankName().matches("")) {
                    bankName = transactionStatusModel.getBankName();
                }

                if (transactionStatusModel.getReferenceNo() != null && !transactionStatusModel.getReferenceNo().matches("")) {
                    referenceNo = transactionStatusModel.getReferenceNo();
                }

                if (transactionStatusModel.getBalanceAmount() != null && !transactionStatusModel.getBalanceAmount().matches("")) {
                    balance = transactionStatusModel.getBalanceAmount();
                    if (balance.contains(":")) {
                        String[] separated = balance.split(":");
                        balance = separated[1].trim();
                    }
                }

                if (transactionStatusModel.getTransactionAmount() != null && !transactionStatusModel.getTransactionAmount().matches("")) {
                    amount = transactionStatusModel.getTransactionAmount();
                }
                ll_maiin.setBackgroundColor(Color.parseColor("#D94237"));
                status_icon.setImageResource(R.drawable.ic_errorrr);
                backBtn.setBackgroundResource(R.drawable.button_background_fail);
                downloadBtn.setBackgroundResource(R.drawable.button_background_fail);
                printBtn.setBackgroundResource(R.drawable.button_background_fail);
                statusMsgTxt.setText("Failed");
                statusDescTxt.setText(transactionStatusModel.getApiComment());

                if (transactionStatusModel.getTransactionType().equalsIgnoreCase("Cash Withdrawal")) {
                    //ll13.setVisibility(View.VISIBLE);
                    txnID.setText(txnid);
                    rref_num.setText(referenceNo);
                    aadhar_number.setText(aadharCard);
                    bank_name.setText(bankName);
                    card_amount.setText(balance);
                    card_transaction_amount.setText(amount);
                    card_transaction_type.setText(transactionStatusModel.getTransactionType());

                } else if (transactionStatusModel.getTransactionType().equalsIgnoreCase("Balance Enquery") ||transactionStatusModel.getTransactionType().equalsIgnoreCase("Balance Enquiry")) {
                    txnID.setText(txnid);
                    rref_num.setText(referenceNo);
                    aadhar_number.setText(aadharCard);
                    bank_name.setText(bankName);
                    card_amount.setText(balance);
                    card_transaction_amount.setText("N/A");
                    card_transaction_type.setText("Balance Enquiry");

                }
            }
        }
        backBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                finish();
            }
        });

        downloadBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checker.lacksPermissions(REQUIRED_PERMISSION)) {
                    PermissionsActivity.startActivityForResult(TransactionStatusNewActivity.this, PERMISSION_REQUEST_CODE, REQUIRED_PERMISSION);
                } else {
                    Date date = new Date();
                    long timeMilli = date.getTime();
                    System.out.println("Time in milliseconds using Date class: " + String.valueOf(timeMilli));
                    createPdf(FileUtils.getAppPath(mContext) + String.valueOf(timeMilli)+"Order_Receipt.pdf");
                }
            }
        });
        printBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BluetoothDevice bluetoothDevice = SdkConstants.bluetoothDevice;
                if (bluetoothDevice != null) {

                    if (!B.isEnabled()) {
                        Toast.makeText(getApplicationContext(), "Your Bluetooth is OFF .",Toast.LENGTH_LONG).show();
                    } else {
                        callBluetoothFunction(txnID.getText().toString(), aadhar_number.getText().toString(), date_time.getText().toString(), bank_name.getText().toString(), rref_num.getText().toString(),card_amount.getText().toString(), card_transaction_amount.getText().toString(), card_transaction_type.getText().toString(), bluetoothDevice);
                    }
                } else {

                    Toast.makeText(getApplicationContext(), "Please connect the printer",Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    public void createPdf(String dest) {

        if (new File(dest).exists()) {
            new File(dest).delete();
        }

        try {
            /**
             * Creating Document
             */
            Document document = new Document();

            // Location to save
            PdfWriter.getInstance(document, new FileOutputStream(dest));

            // Open to write
            document.open();

            // Document Settings
            document.setPageSize(PageSize.A4);
            document.addCreationDate();
            document.addAuthor("");
            document.addCreator("");

            /**
             * How to USE FONT....
             */
            BaseFont urName = BaseFont.createFont("assets/fonts/brandon_medium.otf", "UTF-8", BaseFont.EMBEDDED);

            // LINE SEPARATOR
            LineSeparator lineSeparator = new LineSeparator();
            lineSeparator.setLineColor(new BaseColor(0, 0, 0, 68));

            BaseFont bf = BaseFont.createFont(
                    BaseFont.TIMES_ROMAN,
                    BaseFont.CP1252,
                    BaseFont.EMBEDDED);
            Font font = new Font(bf, 30);
            Font font2 = new Font(bf, 26);

            PdfPTable table = new PdfPTable(2);
            table.setWidthPercentage(100);
            PdfPCell cell = new PdfPCell(new Paragraph("Transaction Receipt",font));
            cell.setColspan(2); // colspan
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(cell);

            table.addCell(new Paragraph("Transaction ID",font)); // how to change cell to have different font and bold and background color
            table.addCell(new Paragraph(txnID.getText().toString(),font2)); // how to change cell to have different font and bold and background color
            table.addCell(new Paragraph("Aadhar Number",font));
            table.addCell(new Paragraph(aadhar_number.getText().toString(),font2));
            table.addCell(new Paragraph("Date/Time",font));
            table.addCell(new Paragraph(date_time.getText().toString(),font2));
            table.addCell(new Paragraph("Bank Name",font));
            table.addCell(new Paragraph(bank_name.getText().toString(),font2));
            table.addCell(new Paragraph("RRN NO",font));
            table.addCell(new Paragraph(rref_num.getText().toString(),font2));
            table.addCell(new Paragraph("Balance Amount",font));
            table.addCell(new Paragraph(card_amount.getText().toString(),font2));
            table.addCell(new Paragraph("Transaction Amount",font));
            table.addCell(new Paragraph(card_transaction_amount.getText().toString(),font2));
            table.addCell(new Paragraph("Transaction Type",font));
            table.addCell(new Paragraph(card_transaction_type.getText().toString(),font2));
            document.add(table);

            // Title Order Details...
            // Adding Title....
            Font mOrderDetailsTitleFont;
            if(statusMsgTxt.getText().toString().equalsIgnoreCase("FAILED")){
                mOrderDetailsTitleFont = new Font(urName, 40.0f, Font.NORMAL, BaseColor.RED);

            }else{
                mOrderDetailsTitleFont = new Font(urName, 40.0f, Font.NORMAL, BaseColor.GREEN);
            }

            Chunk mOrderDetailsTitleChunk = new Chunk(statusMsgTxt.getText().toString(), mOrderDetailsTitleFont);
            Paragraph mOrderDetailsTitleParagraph = new Paragraph(mOrderDetailsTitleChunk);
            mOrderDetailsTitleParagraph.setAlignment(Element.ALIGN_CENTER);
            document.add(mOrderDetailsTitleParagraph);
            document.close();

            Toast.makeText(mContext, "PDF saved in the internal storage", Toast.LENGTH_SHORT).show();

            // FileUtils.openFile(mContext, new File(dest));
            Intent intent = new Intent(TransactionStatusNewActivity.this, ShowPDFActivity.class);
            intent.putExtra("filePath",dest);
            startActivity(intent);

        } catch (IOException | DocumentException ie) {
            Log.e("createPdf: Error ","" + ie.getLocalizedMessage());
        } catch (ActivityNotFoundException ae) {
            Toast.makeText(mContext, "No application found to open this file.", Toast.LENGTH_SHORT).show();
        }
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == PermissionsActivity.PERMISSIONS_GRANTED) {
            Toast.makeText(mContext, "Permission Granted to Save", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(mContext, "Permission not granted, Try again!", Toast.LENGTH_SHORT).show();
        }

    }

    private void callBluetoothFunction(final String txnId, final String aadharNo, final String date, final String bank_name, final String reffNo, final String amount, final String transactionAmt, final String type, BluetoothDevice bluetoothDevice) {


        final BluetoothPrinter mPrinter = new BluetoothPrinter(bluetoothDevice);
        mPrinter.connectPrinter(new BluetoothPrinter.PrinterConnectListener() {

            @Override
            public void onConnected() {
                mPrinter.addNewLine();
                mPrinter.setAlign(BluetoothPrinter.ALIGN_CENTER);
                mPrinter.setBold(true);
                mPrinter.addNewLine();
                mPrinter.setAlign(BluetoothPrinter.ALIGN_CENTER);
                mPrinter.printText("-----Transaction Report-----");
                mPrinter.addNewLine();
                mPrinter.setAlign(BluetoothPrinter.ALIGN_CENTER);
                mPrinter.setBold(true);
                mPrinter.printText(statusMsgTxt.getText().toString());
                mPrinter.addNewLine();
                mPrinter.addNewLine();
                mPrinter.printText("TXNId: "+ txnID.getText().toString());
                mPrinter.addNewLine();
                mPrinter.printText("Aadhar Number: "+aadharNo);
                mPrinter.addNewLine();
                mPrinter.printText("Date/Time: "+date);
                mPrinter.addNewLine();
                mPrinter.printText("Bank Name.: "+bank_name);
                mPrinter.addNewLine();
                mPrinter.printText("Rrn No.: "+reffNo);
                mPrinter.addNewLine();
                mPrinter.printText("Balance Amount: "+amount);
                mPrinter.addNewLine();
                mPrinter.printText("Transaction Amount: "+transactionAmt);
                mPrinter.addNewLine();
                mPrinter.printText("TransactionType: "+type);
                mPrinter.addNewLine();
                mPrinter.addNewLine();
                mPrinter.setBold(true);
                mPrinter.setAlign(BluetoothPrinter.ALIGN_RIGHT);
                mPrinter.printText("Thank You");
                mPrinter.addNewLine();
                mPrinter.setAlign(BluetoothPrinter.ALIGN_RIGHT);
                mPrinter.printText("-----------------------------------");
                mPrinter.addNewLine();
                mPrinter.addNewLine();
                mPrinter.finish();
            }
            @Override
            public void onFailed() {
                Log.d("BluetoothPrinter", "Connection failed");
//                finish();
                Toast.makeText(TransactionStatusNewActivity.this, "Please switch on bluetooth printer", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public  boolean isStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                System.out.println("Permission is granted");
                return true;
            } else {

                System.out.println("Permission is revoked");
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, STORAGE_PERMISSION_CODE);
                return false;
            }
        }
        else {
            //permission is automatically granted on sdk upon installation
            System.out.println("Permission is granted");
            return true;
        }
    }
    public void requestPermission(){
        if(ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)){
            new AlertDialog.Builder(this)
                    .setTitle("")
                    .setMessage("")
                    .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            Toast.makeText(getApplicationContext(),
                                    "You clicked on OK", Toast.LENGTH_SHORT).show();
                        }
                    }).show();
        }else {
            ActivityCompat.requestPermissions(this, new String[] {Manifest.permission.READ_EXTERNAL_STORAGE},STORAGE_PERMISSION_CODE);
        }


    }
}



